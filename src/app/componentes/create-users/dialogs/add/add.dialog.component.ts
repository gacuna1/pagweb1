import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject, OnInit } from '@angular/core';
import { FormArray, FormBuilder,FormControl, FormGroup, Validators } from '@angular/forms';

import { list_new_user } from '../../../../modules/list_new_user';

import { InterfaceCountry } from 'src/app/interface/person/interface-country';
import { Role } from 'src/app/interface/person/role';
import { TypePerson } from 'src/app/interface/person/type-person';
import { Email } from 'src/app/interface/person/email';
import { Account } from 'src/app/interface/account/account';
import { Place } from 'src/app/interface/account/place';

// SERVICIOS
import { ServicieCountryService } from '../../../../services/person/country/servicie-country.service';
import { RoleService } from '../../../../services/person/roles/role.service';
import { TypePersonService } from '../../../../services/person/type_person/type-person.service';
import { EmailService } from '../../../../services/person/correo/email.service';
import { AccountService } from  '../../../../services/accounts/account.service'
import { PlacesService } from '../../../../services/places/places.service';
import { ListUserService } from 'src/app/services/person/list_user/list-user.service';



@Component({
  selector: 'imp-add.dialog',
  templateUrl: './add.dialog.component.html',
  styleUrls: ['./add.dialog.component.scss']
})
export class AddDialogComponent implements OnInit {

  list_user : list_new_user [];
  data_place: Place [];
  data_account: Account [];
  countries: InterfaceCountry [];
  roles: Role [];
  type_persons: TypePerson [];
  vali_correo: Email [];

  new_user = this.formBuilder.group({

    first_name: ['',[
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(50)
    ]],
    middename: ['',[
      Validators.minLength(3),
      Validators.maxLength(50)
    ]],
    last_name: ['',[
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(50)
    ]],
    seglastname: ['',[
      Validators.minLength(3),
      Validators.maxLength(50)
    ]],
    email: ['',[
      Validators.required,
      Validators.minLength(10),
      Validators.maxLength(320) // TODO: PENDIENTE DE REVSIÓN DE SI SE NECESITA EL CORREO Y STATUS EN LA RESPUESTA DEL SERIVICIO buscarPorEmail
      // Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$/gm)
      // Validators.email
    ]],
    country_id: ['',[,
      Validators.required
    ]],
    rol_user: ['',[
      Validators.required
    ]],
    type_user: ['',[
      Validators.required
    ]]
    // ,
    // account_store: this.formBuilder.array([
    //   this.account_store()
    // ])
    });

    get account_id(){
      return this.new_user.get('account_store') as FormArray;
    }

    account_store(): FormGroup {
      return this.formBuilder.group({
        account_id: ['',Validators.required],
        stores: this.formBuilder.array([
          this.formBuilder.group({
          store_id: ['']
        })
        ])
      })
    }

  constructor(
    private formBuilder: FormBuilder,
    private ServicieCountryService: ServicieCountryService,
    private RoleService: RoleService,
    private TypePersonService: TypePersonService,
    private EmailService: EmailService,
    private AccountService: AccountService,
    private PlacesService: PlacesService,
    private ListUserService: ListUserService,
    
    
    public dialogRef: MatDialogRef<AddDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: list_new_user
              ) { }

  addAccounts(): void {
    (<FormArray>this.new_user.get('account_store')).push(
      this.formBuilder.group({
        account_id: ['',Validators.required],
        stores: this.formBuilder.array([
          this.formBuilder.group({
          store_id: ['']
        })
        ])
      })
    )
  }

  submit() {
  // emppty stuff
  }

  public confirmAdd(): void {
    // this.dataService.addIssue(this.data);
  }

  ngOnInit(): void {
    this.ServicieCountryService.getcountry().subscribe(async data_country => {this.countries = await data_country, console.log(this.countries)});

    this.RoleService.getRoles().subscribe(async data_roles => {this.roles = await data_roles, console.log(this.roles)});
    
    // this.ListUserService.getListUsers().then(async arrUsers => { this.list_user = await arrUsers, console.log(this.list_user)});
  }

  setProjects(x) {
    let arr = new FormArray([])
    x.projects.forEach(y => {
      arr.push(this.formBuilder.group({ 
        store_id: y.stores 
      }))
    })
    return arr;
  }

  addNewPlace(control_new_store) {
    control_new_store.push(
      this.formBuilder.group({
        store_id: ['']
      }))
  }

  removeAccountButton(account_storeGroupIndex: number): void {
    console.log(account_storeGroupIndex);
    (<FormArray>this.new_user.get('account_store')).removeAt(account_storeGroupIndex);
  }

  removePlaceButton(control_store, Group_store_index: number): void {
    console.log(control_store);
    console.log(Group_store_index);
    control_store.removeAt(Group_store_index);
  }

  get_All_Stores(account_id_allStores: number){
    console.log(account_id_allStores);
  }
  
  getType_user(id_country){
    console.log(id_country);
    this.TypePersonService.getcountry(id_country).subscribe(async data_type_users => {this.type_persons = await data_type_users, console.log(this.type_persons)});
  }

  get_email(email_enviado: string){
    console.log('email_envido ',email_enviado)
    this.EmailService.getEmail(email_enviado).subscribe(async data_valida_correo => {this.vali_correo = await data_valida_correo, console.log(this.vali_correo)});
  }

  get_account(name_account: string, country_id: number){
    console.log('nombre cuenta 1',name_account);
    console.log('nombre cuenta ',encodeURIComponent(name_account));
    console.log('id_pais ',country_id);
   
    this.AccountService.getAccount(country_id,encodeURIComponent(name_account)).subscribe(async data_exis_account => {this.data_account = await data_exis_account, console.log(this.data_account)});
  }

  get_place(account: number,name_place: string){
    console.log(encodeURIComponent(name_place));
    console.log(account);
   
    this.PlacesService.getPlace(account,encodeURIComponent(name_place)).subscribe(async data_exis_place => {this.data_place = await data_exis_place, console.log(this.data_place)});
  }

  confirm_new_user(){
    this.ListUserService.insert(this.new_user.value)
    console.log(this.new_user)
  }

  cancel_new_user(): void {
    this.dialogRef.close();
  }

  onSubmit() {}
}
