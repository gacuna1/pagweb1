// ANGULAR
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { fromEvent, Observable } from 'rxjs';
import { DataSource } from '@angular/cdk/collections';

// MODULES
import { list_new_user } from '../../modules/list_new_user';

// INTERFACES
import { InterfaceCountry } from 'src/app/interface/person/interface-country';
import { Role } from 'src/app/interface/person/role';
import { TypePerson } from 'src/app/interface/person/type-person';
import { Email } from 'src/app/interface/person/email';
import { Account } from 'src/app/interface/account/account';
import { Place } from 'src/app/interface/account/place';
import { new_list_user } from 'src/app/interface/person/list_user';

// SERVICIOS
import { ServicieCountryService } from '../../services/person/country/servicie-country.service';
import { RoleService } from '../../services/person/roles/role.service';
import { TypePersonService } from '../../services/person/type_person/type-person.service';
import { EmailService } from '../../services/person/correo/email.service';
import { AccountService } from  '../../services/accounts/account.service'
import { PlacesService } from '../../services/places/places.service';
import { ListUserService } from 'src/app/services/person/list_user/list-user.service';

// DIALOG
import { AddDialogComponent } from "../create-users/dialogs/add/add.dialog.component";

// ANGULAR MATERIAL
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';


@Component({
  selector: 'imp-create-users',
  templateUrl: './create-users.component.html',
  styleUrls: ['./create-users.component.scss']
})
export class CreateUsersComponent implements OnInit {


  user_database: ListUserService | null;
  //dataSource: user_dataSource | null;
  dataSource ;

  refresh(){
    this.ListUserService.getListUsers().subscribe(async arrUsers => {
      this.dataSource = await arrUsers,
      this.list_user = await arrUsers,
      console.log('this.dataSource ',this.dataSource)
      console.log('this.list_user 2222',this.list_user)
    });
  };

  displayedColumns = ['first_name','middename','last_name','seglastname','email','country_id','rol_user','type_user'
  //,'account','store',
  ,'actions'];
  // dataSource;

  loadData() {
    this.ListUserService.getListUsers().subscribe(async arrUsers => {
      this.list_user = await arrUsers,
      console.log('lisadsilsa',this.list_user)
    });
  }
  
  addNew() {
    const dialogRef = this.dialog.open(AddDialogComponent, {
      data: {list_new_user: list_new_user }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.refresh()
        // After dialog is closed we're doing frontend updates
        // For add we're just pushing a new row inside DataService
        // this.user_database.dataChange.value.push(this.ListUserService.getDialogData());
        //this.refreshTable();

        // this.ListUserService.getListUsers().then(async arrUsers => { this.list_user = await arrUsers, console.log('aaca afterClosed ',this.list_user)});
        // this.dataSource = this.list_user;
      }
    });
  }

  private refreshTable_users() {
    // this.ListUserService.getListUsers().then(async arrUsers => { this.list_user = await arrUsers, console.log(this.list_user)}, this.dataSource = this.list_user);
  }


  list_user : list_new_user [];
  data_place: Place [];
  data_account: Account [];
  countries: InterfaceCountry [];
  roles: Role [];
  type_persons: TypePerson [];
  vali_correo: Email [];


  new_user = this.formBuilder.group({
    
    first_name: ['',[
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(50)
    ]],
    middename: ['',[
      Validators.minLength(3),
      Validators.maxLength(50)
    ]],
    last_name: ['',[
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(50)
    ]],
    seglastname: ['',[
      Validators.minLength(3),
      Validators.maxLength(50)
    ]],
    email: ['',[
      Validators.required,
      Validators.minLength(10),
      Validators.maxLength(320) // TODO: PENDIENTE DE REVSIÓN DE SI SE NECESITA EL CORREO Y STATUS EN LA RESPUESTA DEL SERIVICIO buscarPorEmail
      // Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$/gm)
      // Validators.email
    ]],
    country_id: ['',[,
      Validators.required
    ]],
    rol_user: ['',[
      Validators.required
    ]],
    type_user: ['',[
      Validators.required
    ]],
    account_store: this.formBuilder.array([
      this.account_store()
    ])
   });

  get account_id(){
    return this.new_user.get('account_store') as FormArray;
  }

  constructor(
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private ServicieCountryService: ServicieCountryService,
    private RoleService: RoleService,
    private TypePersonService: TypePersonService,
    private EmailService: EmailService,
    private AccountService: AccountService,
    private PlacesService: PlacesService,
    private ListUserService: ListUserService) {}
    
    addAccounts(): void {
      (<FormArray>this.new_user.get('account_store')).push(
        this.formBuilder.group({
          account_id: ['',Validators.required],
          stores: this.formBuilder.array([
            this.formBuilder.group({
            store_id: ['']
          })
          ])
        })
      )
    }

    ngOnInit(): void {
      // this.ServicieCountryService.getcountry().subscribe(async data_country => {
      //   this.countries = await data_country,
      //   console.log(this.countries)
      // });
  
      // this.RoleService.getRoles().subscribe(async data_roles => {
      //   this.roles = await data_roles,
      //   console.log(this.roles)
      // });

      // this.ListUserService.getListUsers().subscribe(async arrUsers => {
      //   this.dataSource.data = await arrUsers,
      //   console.log(this.dataSource)
      // });

      // this.ListUserService.getListUsers().subscribe(async arrUsers => {
      //   this.dataSource = await arrUsers,
      //   console.log('this.dataSource ',this.dataSource)
      // });

      // this.ListUserService.getListUsers().subscribe(async arrUsers => {this.list_user = await arrUsers, console.log('aaca ngOnInit ',this.list_user)});
  
    }


  setProjects(x) {
    let arr = new FormArray([])
    x.projects.forEach(y => {
      arr.push(this.formBuilder.group({ 
        store_id: y.stores 
      }))
    })
    return arr;
  }
  account_store(): FormGroup {
    return this.formBuilder.group({
      account_id: ['',Validators.required],
      stores: this.formBuilder.array([
        this.formBuilder.group({
        store_id: ['']
      })
      ])
    })
  }

  addNewPlace(control_new_store) {
    control_new_store.push(
      this.formBuilder.group({
        store_id: ['']
      }))
  }

  removeAccountButton(account_storeGroupIndex: number): void {
    console.log(account_storeGroupIndex);
    (<FormArray>this.new_user.get('account_store')).removeAt(account_storeGroupIndex);
  }

  removePlaceButton(control_store, Group_store_index: number): void {
    console.log(control_store);
    console.log(Group_store_index);
    control_store.removeAt(Group_store_index);
  }

  get_All_Stores(account_id_allStores: number){
    console.log(account_id_allStores);
  }
  

  
  getType_user(id_country){
    console.log(id_country.target.value);
    this.TypePersonService.getcountry(id_country.target.value).subscribe(async data_type_users => {this.type_persons = await data_type_users, console.log(this.type_persons)});
  }

  get_email(email_enviado: string){
    console.log('email_envido ',email_enviado)
    this.EmailService.getEmail(email_enviado).subscribe(async data_valida_correo => {this.vali_correo = await data_valida_correo, console.log(this.vali_correo)});
  }

  get_account(name_account: string, country_id: number){
    console.log('nombre cuenta 1',name_account);
    console.log('nombre cuenta ',encodeURIComponent(name_account));
    console.log('id_pais ',country_id);
   
    this.AccountService.getAccount(country_id,encodeURIComponent(name_account)).subscribe(async data_exis_account => {this.data_account = await data_exis_account, console.log(this.data_account)});
  }

  get_place(account: number,name_place: string){
    console.log(encodeURIComponent(name_place));
    console.log(account);
   
    this.PlacesService.getPlace(account,encodeURIComponent(name_place)).subscribe(async data_exis_place => {this.data_place = await data_exis_place, console.log(this.data_place)});
  }


  list_user_table(){
    this.ListUserService.insert(this.new_user.value)
    console.log(this.list_user)
  }


  onSubmit() {}
}


// // this.ListUserService.getListUsers().then(async arrUsers => { this.list_user = await arrUsers, console.log(this.list_user)});

// export class user_dataSource extends DataSource<list_new_user> {

//   renderedData: list_new_user[] = [];

//   constructor(
//     public user_dataSource: ListUserService) {
//       super();
//   }

//   /** Connect function called by the table to retrieve one stream containing the data to render. */
//   connect(): Observable<list_new_user[]> {

//     const displayDataChanges = [
//       this.user_dataSource.dataChange
//     ];

//     this.user_dataSource.getListUsers();

//     return merge(...displayDataChanges).pipe(map( () => {
//       // Filter data
//       this.filteredData = this._exampleDatabase.data.slice().filter((issue: Issue) => {
//         const searchStr = (issue.id + issue.title + issue.url + issue.created_at).toLowerCase();
//         return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
//       });

//     }

//   }

//   disconnect() {}


// }
