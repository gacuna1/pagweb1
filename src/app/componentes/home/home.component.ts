import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { modulo } from 'src/app/modules/modulo';

@Component({
  selector: 'imp-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  @Output() moduloSeleccionado: EventEmitter<modulo>

  arrModulos: modulo[] = [{
      "title":"new_users",
      "imagen":"image"
    }
  ]

  constructor(private router: Router){}

  ngOnInit(): void {
  }
  
  onClick( pModule ){
    this.router.navigate([pModule])
  }
}