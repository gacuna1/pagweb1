import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Account } from 'src/app/interface/account/account';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  constructor(private httpClient: HttpClient) {}

  getAccount(country_id, name_account): Observable<Account[]> {
    return this.httpClient.get<Account[]>('/account/' + country_id + '/' + name_account);
    // return this.httpClient.get(environment.ruta_base+'/roles')
  }
}
