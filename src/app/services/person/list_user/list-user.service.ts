import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable, of} from 'rxjs';
import { list_new_user } from 'src/app/modules/list_new_user';
import { new_list_user } from 'src/app/interface/person/list_user';

@Injectable({
  providedIn: 'root'
})
export class ListUserService {

  dataChange: BehaviorSubject<list_new_user[]> = new BehaviorSubject<list_new_user[]>([]);

  arrUsers: list_new_user[];
  dialogData: any;

  constructor() { 
    this.arrUsers =[];
  }

  getDialogData() {
    return this.dialogData;
  }

  get data(): list_new_user[] {
    return this.dataChange.value;
  }

  // getListUsers(): Observable<new_list_user[]>{
  //   return this.arrUsers. 
  // }



  // getListUsers(): Promise<list_new_user[]>{
  //   return new Promise((resolve, reject) => {
  //     resolve(this.arrUsers);
  //   })
  // }


  getListUsers(): Observable<list_new_user[]>{
    return of(this.arrUsers)
  }


  insert(list_new_user: list_new_user): void {
    console.log('aca',list_new_user);
    // this.dialogData = list_new_user;
    this.arrUsers.push(list_new_user);
    // console.log('aca2',this.dialogData);
    console.log('aca2',this.arrUsers);
  }
}
