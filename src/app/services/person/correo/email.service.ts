import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Email } from 'src/app/interface/person/email';

@Injectable({
  providedIn: 'root'
})
export class EmailService {
  constructor(private httpClient: HttpClient) {}

  getEmail(str_mail): Observable<Email[]> {
    return this.httpClient.get<Email[]>('/person/' + str_mail);
    // return this.httpClient.get(environment.ruta_base+'/roles')
  }
}
