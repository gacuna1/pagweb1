import { TestBed } from '@angular/core/testing';

import { ServicieCountryService } from './servicie-country.service';

describe('ServicieCountryService', () => {
  let service: ServicieCountryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServicieCountryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
