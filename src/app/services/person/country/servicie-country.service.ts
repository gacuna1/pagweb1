import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { InterfaceCountry } from 'src/app/interface/person/interface-country';

@Injectable({
  providedIn: 'root'
})
export class ServicieCountryService {
  constructor(private httpClient:HttpClient) {}

  getcountry(): Observable<InterfaceCountry[]>{
    return this.httpClient.get<InterfaceCountry[]>('/country')
    // return this.httpClient.get(environment.ruta_base+'/roles')
  }
}
