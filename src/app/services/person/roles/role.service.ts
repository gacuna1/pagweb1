import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Role } from 'src/app/interface/person/role';


@Injectable({
  providedIn: 'root'
})
export class RoleService {
  constructor(private httpClient:HttpClient) {}

  getRoles(): Observable<Role[]>{
    return this.httpClient.get<Role[]>('/roles')
    // return this.httpClient.get(environment.ruta_base+'/country')
  }
}