import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TypePerson } from 'src/app/interface/person/type-person';

@Injectable({
  providedIn: 'root',
})
export class TypePersonService {
  constructor(private httpClient: HttpClient) {}

  getcountry(id_country): Observable<TypePerson[]> {
    return this.httpClient.get<TypePerson[]>('/type_user/' + id_country);
    // return this.httpClient.get(environment.ruta_base+'/roles')
  }
}
