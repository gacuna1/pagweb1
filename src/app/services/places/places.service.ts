import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Place } from 'src/app/interface/account/place';

@Injectable({
  providedIn: 'root'
})
export class PlacesService {
  constructor(private httpClient: HttpClient) {}

  getPlace(account_id, name_account): Observable<Place[]> {
    return this.httpClient.get<Place[]>('/place/' + account_id + '/' + name_account);
    // return this.httpClient.get(environment.ruta_base+'/roles')
  }
}
