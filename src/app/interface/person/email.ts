export interface Email {
    email?: string,
    status?: number,
    activo?: boolean
}
