export interface Account {
    id?: number,
    name?: string,
    exists?: boolean
}
