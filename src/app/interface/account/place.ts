export interface Place {
    id?: number,
    place_name?: string,
    exists?: boolean
}
