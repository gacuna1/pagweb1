export class list_new_user {
  first_name: string;
  middename?: string;
  last_name: string;
  seglastname?: string;
  email: string;
  country: number;
  rol_user: number;
  type_user: number;
  // account_store?: {
  //   account_id?: number;
  //   stores?: {
  //     store_id?: number;
  //   };
  // };
}
