const express = require("express");
const router = express.Router();

const list_pais = require("../models/country");

router.get("/country", function (req, res) {
  list_pais
    .country()
    .then((paises) => {
      // console.log(paises.length);
      // console.log(paises[0]);
      return res.json(paises);
    })
    .catch((err) => {
      return res
        .status(500)
        .send("Error consulta solicitada del servicio buscarPorEmail");
    });
});

module.exports = router;