const express = require("express");
const router = express.Router();

const vali_users = require("../models/users");

router.get("/person/:email", function (req, res) {
  vali_users
    .buscarPorEmail(req.params.email)
    .then((resultado) => {
      // console.log(resultado.length)
      if (
        resultado === null ||
        resultado === undefined ||
        resultado.length == 0
      )
        return res.json(false);
      else return res.json(resultado[0]);
    })
    .catch((err) => {
      return res.status(500).send("Error consulta solicitada del servicio buscarPorEmail");
    });
});

module.exports = router;
