const express = require("express");
const router = express.Router();

const list_account = require("../models/account");

router.get("/account/:id_country/:name", function (req, res) {
    list_account
    .buscarAccount(req.params.id_country,req.params.name)
    .then((account_name) => {
      console.log(account_name)
      console.log(account_name[0])
      console.log(account_name.length)
      if (
        account_name === null ||
        account_name === undefined ||
        account_name.length == 0
      )
        return res.json(false);
      else return res.json(account_name[0]);
    })
    .catch((err) => {
      return res.status(500).send("Error consulta solicitada del servicio buscarPorEmail");
    });
});

module.exports = router;
