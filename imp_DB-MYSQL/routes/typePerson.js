const express = require("express");
const router = express.Router();

const vali_users = require("../models/type_person");

router.get("/type_user/:id_country", function (req, res) {
  vali_users
    .buscarTypePerson(req.params.id_country)
    .then((typeuser) => {
      console.log(typeuser.length)
      if (
        typeuser === null ||
        typeuser === undefined ||
        typeuser.length == 0
      )
        return res.json(false);
      else return res.json(typeuser);
    })
    .catch((err) => {
      return res.status(500).send("Error consulta solicitada del servicio buscarTypePerson");
    });
});

module.exports = router;
