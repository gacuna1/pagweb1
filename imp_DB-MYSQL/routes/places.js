const express = require("express");
const router = express.Router();

const list_place = require("../models/places");

router.get("/place/:id_account/:name", function (req, res) {
    list_place
    .buscarAccount(req.params.id_account,req.params.name)
    .then((place_name) => {
      console.log(place_name)
      console.log(place_name[0])
      console.log(place_name.length)
      if (
        place_name === null ||
        place_name === undefined ||
        place_name.length == 0
      )
        return res.json(false);
      else return res.json(place_name[0]);
    })
    .catch((err) => {
      return res.status(500).send("Error consulta solicitada del servicio buscarPorEmail");
    });
});

module.exports = router;
