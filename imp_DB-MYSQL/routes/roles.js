const express = require("express");
const router = express.Router();

const list_rol = require("../models/rol");

router.get("/roles", function (req, res) {
  list_rol
    .roles()
    .then((roles) => {
    //   console.log(roles.length);
    //   console.log(roles[0]);
      return res.json(roles);
    })
    .catch((err) => {
      return res
        .status(500)
        .send("Error consulta solicitada del servicio buscarPorRol");
    });
});

module.exports = router;