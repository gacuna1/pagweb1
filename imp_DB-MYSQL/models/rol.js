const conexion = require("../dbConfig");

module.exports = {
  roles() {
    return new Promise((resolve, reject) => {
      conexion.query(
        `Select idalto_rol, rol_name
        from roles
        where idalto_rol in(2,3,5,7,9,20,23)
        order by order_list asc`,
        (err, roles) => {
        //   console.log(roles);
          if (err) reject(err);
          else resolve(roles);
        }
      );
    });
  },
};
