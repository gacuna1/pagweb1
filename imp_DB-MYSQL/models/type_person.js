const conexion = require('../dbConfig')

module.exports = {
    buscarTypePerson(id_country) {
        return new Promise((resolve, reject) => {
            conexion.query(
                `Select id_person_type, person_type
                from custos.person_type
                where person_type in('ALTO Team','Client','Cliente')
                and id_country = ?
                order by order_list asc`, [id_country], (err, typeuser) => {
                console.log(typeuser);
                if (err) reject(err);
                else 
                resolve(typeuser);
            });
        });
    }
}   