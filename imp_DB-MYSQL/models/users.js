const conexion = require('../dbConfig')

module.exports = {
    buscarPorEmail(email) {
        return new Promise((resolve, reject) => {
            conexion.query(
                `select email,status from person where email = ?`, [email], (err, resultado) => {
                // console.log(resultado);
                if (err) reject(err);
                else 
                resolve(resultado);
            });
        });
    }
}   