const conexion = require("../dbConfig");

module.exports = {
  country() {
    return new Promise((resolve, reject) => {
      conexion.query(
        `select id_country, country
                from country
                where id_country !=0 
                order by id_country asc`,
        (err, paises) => {
          // console.log(paises);
          if (err) reject(err);
          else resolve(paises);
        }
      );
    });
  },
};
