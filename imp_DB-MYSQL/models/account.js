const conexion = require('../dbConfig')

module.exports = {
    buscarAccount(id_country,name) {
        return new Promise((resolve, reject) => {
            conexion.query(
                `Select id,name
                from custos.accounts
                where country_config_id = ?
                and status && active_status && indcustos = 1
                and name = ?`, [id_country,name], (err, account_name) => {
                console.log(account_name);
                if (err) reject(err);
                else 
                resolve(account_name);
            });
        });
    }
}   