const conexion = require('../dbConfig')

module.exports = {
    buscarAccount(id_account,name) {
        return new Promise((resolve, reject) => {
            conexion.query(
                `Select id,place_name
                from places
                where account_id = ?
                and status && active_status =1
                and ind_default is null
                and place_name = ?`, [id_account,name], (err, place_name) => {
                console.log(place_name);
                if (err) reject(err);
                else 
                resolve(place_name);
            });
        });
    }
}   