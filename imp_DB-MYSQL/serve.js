const express = require('express');
const connection = require('./dbConfig');

const user_email = require('./routes/person');
const rut_pais = require('./routes/countries');
const rut_role = require('./routes/roles');
const rut_type_user = require('./routes/typePerson');
const rut_account = require('./routes/accounts');
const rut_place = require('./routes/places');

const app = express();
const port = 8000;

app.get('/', (req, res) => res.send('Hello World!'))

app.post('/', (req, res) => res.send('Hello POST World!'))

app.listen(port, () => console.log(`Example app listening on port ${port}!`))

app.get('/person/:email', user_email);
app.get('/country', rut_pais);
app.get('/roles', rut_role);
app.get('/type_user/:id_country', rut_type_user);
app.get('/account/:id_country/:name', rut_account);
app.get('/place/:id_account/:name', rut_place);

module.exports = app;